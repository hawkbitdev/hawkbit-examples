# README #

This repository contains examples for HawkBit. HawkBit is a Java network engine based on RakNet. More information are available at the [official project site](https://hawkbitdev.wordpress.com/). You can find the API of HawkBit [here](http://hawkbitdev.bitbucket.org/hawkbit/javadoc/index.html). The examples are located in the **com.hawkbit.examples** package in the **src/main/java** folder.

### How to run the examples? ###

1. Clone this repository.
2. Download the needed jars from the [download page](https://hawkbitdev.wordpress.com/downloads/) and insert them in the **libs** directory.
3. Make changes to the examples.
4. Call *./gradlew build* inside the root directory. 
5. Go to **build/distributions** and unzip **hawkbit-examples.zip**. 
6. Run *bin/hawkbit-examples*.

### Having questions or suggestions? ###

Write me a comment [here](https://hawkbitdev.wordpress.com/contact/).