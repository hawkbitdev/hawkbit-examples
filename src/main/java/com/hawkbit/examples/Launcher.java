/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hawkbit.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This launcher could start all examples.
 */
public class Launcher {
    
    private static Launchable[] launchables = new Launchable[]{
        new DiscardServer(),
        new EchoServer(),
        new SimpleClient()
    };
    
    public static void main(String[] args) throws IOException{
        System.out.println("Which one should be started?");
        for(int i = 0; i < launchables.length; ++i){
            System.out.println(i+": "+launchables[i].getClass().getSimpleName());
        }
        System.out.println("Enter the number:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  
        int launcherId = Integer.parseInt(br.readLine());
        launchables[launcherId].start(new String[]{});
    }
}
