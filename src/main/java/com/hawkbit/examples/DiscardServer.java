/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hawkbit.examples;

import com.hawkbit.networking.Configuration;
import com.hawkbit.networking.Network;
import com.hawkbit.networking.Peer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A server which allows incoming connections. 
 * It doesn't do anything if a message arrives.
 */
public class DiscardServer implements Launchable{
    
    public static void main(String[] args){
        new DiscardServer().start(args);
    }

    @Override
    public void start(String[] args) {
        //create a server which listnes to port 6534 for incoming connections
        //up to 10 connections are handled at the same time
        Configuration config = Configuration.createServer(6534, 10);
        Peer server = Network.createPeer(config);
        
        //start listening for incoming connections
        server.start(true);
        
        //wait until 'enter' is pressed
        System.out.println("DiscardServer is running. Press 'enter' to quit.");
        try {
            System.in.read();
        } catch (IOException ex) {
            Logger.getLogger(DiscardServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //close all connections and stop listening for new incoming connections
        server.stop();
        
        //destroy the server instance
        server.destroy();
    }
}
