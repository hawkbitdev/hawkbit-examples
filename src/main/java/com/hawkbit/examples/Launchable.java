/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hawkbit.examples;

/**
 * Defines a class that could be launched by calling {@link #start(java.lang.String[]) }.
 * 
 */
public interface Launchable {
    
    /**
     * Starts the instance of the class.
     * 
     * @param args 
     */
    public void start(String[] args);
    
}
