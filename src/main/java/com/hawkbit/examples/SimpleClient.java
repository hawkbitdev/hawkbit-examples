/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hawkbit.examples;

import com.hawkbit.networking.Configuration;
import com.hawkbit.networking.Connection;
import com.hawkbit.networking.ConnectionListener;
import com.hawkbit.networking.MessageIdentifier;
import com.hawkbit.networking.MessageListener;
import com.hawkbit.networking.Network;
import com.hawkbit.networking.PacketPriority;
import com.hawkbit.networking.PacketReliability;
import com.hawkbit.networking.Peer;
import com.hawkbit.networking.Target;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A client which sends console input to the connected peer and prints its response.
 */
public class SimpleClient implements Launchable{
    
    public static void main(String[] args){
        new SimpleClient().start(args);
    }
    
    private Connection connection;

    @Override
    public void start(String[] args) {
        //create a simple client
        Configuration config = Configuration.createClient();
        Peer client = Network.createPeer(config);
        
        //listen for connections
        client.addConnectionListener(new ConnectionListener() {

            @Override
            public void connectionAdded(Peer peer, Connection conn) {
                System.out.println("Connection esthablished");
                System.out.println("Waiting for input (enter \"q\" to quit)...");
                connection = conn;
            }

            @Override
            public void connectionRemoved(Peer peer, Connection conn) {
                System.out.println("Disconnected");
            }
        });
        
        //listen for messages
        client.addMessageListener(new MessageListener() {

            @Override
            public void messageReceived(Connection source, int id, byte[] data) {
                try {
                    System.out.println("Message received with id "+id+": "+new String(data, "UTF-8"));
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(SimpleClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        //start the client
        client.start(true);
        
        //connect to a direct reachable peer
        Target target = Target.createDirect("localhost", 6534);
        client.connect(target);
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));        
        while(true){
            //read input
            String line = "q";
            try {
                line = br.readLine();
            } catch (IOException ex) {
                Logger.getLogger(SimpleClient.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //quit on "quit" or "q"
            if(line.equals("quit") || line.equals("q")){
                break;
            }
            
            //don't send anything until connected
            if(connection == null){
                continue;
            }
            
            //send the read input
            connection.send(MessageIdentifier.getMinUserId(), 
                    line.getBytes(Charset.forName("UTF-8")), 
                    PacketPriority.HIGH_PRIORITY, PacketReliability.RELIABLE);
        }
        
        //disconnect and stop listening for connections and messages
        client.stop();
        
        //destroy the client instance
        client.destroy();
    }
}
