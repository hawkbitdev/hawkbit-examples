/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hawkbit.examples;

import com.hawkbit.networking.Configuration;
import com.hawkbit.networking.Connection;
import com.hawkbit.networking.MessageListener;
import com.hawkbit.networking.Network;
import com.hawkbit.networking.PacketPriority;
import com.hawkbit.networking.PacketReliability;
import com.hawkbit.networking.Peer;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * When a message arrives the EchoServer sends the message back to the client.
 */
public class EchoServer implements Launchable{
    
    public static void main(String[] args){
        new EchoServer().start(args);
    }

    @Override
    public void start(String[] args) {
        //create a server that listenes on port 6534 for connections and allows 
        //10 connections at the same time.
        Configuration config = Configuration.createServer(6534, 10);
        Peer server = Network.createPeer(config);
        
        //listen for messages
        server.addMessageListener(new MessageListener() {

            @Override
            public void messageReceived(Connection source, int id, byte[] data) {
                System.out.println("Returning message with id "+id+" to "
                        +source.getAddress()+": "+Arrays.toString(data));
                //send the message back to its source
                source.send(id, data, PacketPriority.HIGH_PRIORITY, PacketReliability.RELIABLE);
            }
        });
        
        //start the server
        server.start(true);
        
        //wait until 'enter' is pressed
        System.out.println("EchoServer is running. Press 'enter' to quit.");
        try {
            System.in.read();
        } catch (IOException ex) {
            Logger.getLogger(EchoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //stop listening for messages
        server.stop();
        
        //destroy the server instance
        server.destroy();    
    }
    
}
